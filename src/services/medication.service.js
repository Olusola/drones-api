const { Medication } = require("../models");
const ApiError = require("../helpers/ApiError");
const cloudinaryHelper = require("../helpers/cloudinary");

const createMedication = async (req) => {
  try {
    let images;
    if (req.files) {
      images = await processImages(req);
    }
    const data = { ...req.body, images };
    const medication = await Medication.create(data);
    return JSON.parse(JSON.stringify(medication));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const processImages = async (req) => {
  const images = await cloudinaryHelper.uploadImage(Object.values(req.files));
  return images;
};

const findOne = async (criteria) => {
  try {
    const medication = await Medication.findOne({ ...criteria });
    if (!medication || medication.isDeleted) {
      throw new ApiError(404, "Medication not found");
    }
    return JSON.parse(JSON.stringify(medication));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const fetchAll = async (criteria = {}, options = {}) => {
  const { sort = { createdAt: -1 }, limit, page } = options;

  const _limit = parseInt(limit, 10);
  const _page = parseInt(page, 10);

  let medication = await Medication.find(criteria)
    .sort(sort)
    .limit(_limit)
    .skip(_limit * (_page - 1));

  return { medication, page: _page };
};

const count = async (criteria = {}) => {
  return await Medication.find(criteria).countDocuments();
};

const updateMedication = async (id, req) => {
  let medication = await Medication.findById(id);
  if (!medication || medication.isDeleted) {
    throw new ApiError(404, "Medication not found");
  }
  let data = req.body;
  if (req.files) {
    images = await processImages(req);
    data = { ...data, images };
  }

  Object.assign(medication, data);
  await medication.save();
  return medication;
};

const deleteMedication = async (id) => {
  const medication = await Medication.findById(id);
  if (!medication) {
    throw new ApiError(404, "Medication not found");
  }

  Object.assign(medication, { isDeleted: true });
  await medication.save();
  return medication;
};

module.exports = {
  createMedication,
  findOne,
  processImages,
  fetchAll,
  count,
  updateMedication,
  deleteMedication,
};
