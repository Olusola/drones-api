const { Drone, Dispatch, Medication, Audit } = require("../models");
const ApiError = require("../helpers/ApiError");

const createDispatch = async (data) => {
  try {
    const drone = await Drone.findById(data.drone);
    if (drone.state == 'LOADING') {
      throw new ApiError(400, "Drone is already in use");
    }
    if (drone.batteryLevel < 25) {
      throw new ApiError(400, "Battery Level of selected drone is below 25%");
    }
    const dispatchWeight = await getTotalWeight(data.medication)
    if (dispatchWeight > drone.maxWeight) {
      throw new ApiError(400, "Medication exceeds maximum weight of selected drone");
    }
    Object.assign(drone, { state: "LOADING" });
    await drone.save();
    const dispatch = await Dispatch.create(data);
    return JSON.parse(JSON.stringify(dispatch));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const getTotalWeight = async (medication) => {
  try {
    const medications = []
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < medication.length; i += 1) {
      let med = await Medication.findById(medication[i]);
      medications.push(med.weight)
    }
    let total = 0;
    medications.map((item) => {
      return (total = total + parseInt(item));
    });
    return total;
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const findOne = async (criteria) => {
  try {
    const dispatch = await Dispatch.findOne({ ...criteria });
    if (!dispatch || dispatch.isDeleted) {
      throw new ApiError(404, "Dispatch not found");
    }
    return JSON.parse(JSON.stringify(dispatch));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};


const fetchAll = async (criteria = {}, options = {}) => {
  const { sort = { createdAt: -1 }, limit, page } = options;

  const _limit = parseInt(limit, 10);
  const _page = parseInt(page, 10);

  let dispatch = await Dispatch.find(criteria)
    .sort(sort)
    .limit(_limit)
    .populate("medication")
    .skip(_limit * (_page - 1));

  return { dispatch, page: _page };
};

const count = async (criteria = {}) => {
  return await Dispatch.find(criteria).countDocuments();
};

const deleteDispatch = async (id) => {
  const dispatch = await Dispatch.findById(id);
  if (!dispatch) {
    throw new ApiError(404, "Dispatch not found");
  }

  Object.assign(dispatch, { isDeleted: true });
  await dispatch.save();
  return dispatch;
};

const decreaseBatteryLevel = async () => {
  try {
    const drones = await Drone.find({ "status": { $ne: "IDLE" }, isDeleted: false })
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < drones.length; i += 1) {
      let drone = drones[i]
      drone.batteryLevel -= 1
      await drone.save()
    }
    return;
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const logBatteryLevel = async () => {
  try {
    const drones = await Drone.find({ isDeleted: false })
    /* eslint-disable no-await-in-loop */
    for (let i = 0; i < drones.length; i += 1) {
      let drone = drones[i]
      await Audit.create({ drone: drone._id, batteryLevel: drone.batteryLevel });
    }
    return;
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

module.exports = {
  createDispatch,
  findOne,
  fetchAll,
  count,
  deleteDispatch,
  decreaseBatteryLevel,
  logBatteryLevel,
};
