module.exports.droneService = require("./drone.service");
module.exports.medicationService = require("./medication.service");
module.exports.dispatchService = require("./dispatch.service");
module.exports.auditService = require("./audit.service");