const { Drone } = require("../models");
const ApiError = require("../helpers/ApiError");

const createDrone = async (data) => {
  try {
    const drone = await Drone.create(data);
    return JSON.parse(JSON.stringify(drone));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const findOne = async (criteria) => {
  try {
    const drone = await Drone.findOne({ ...criteria });
    if (!drone || drone.isDeleted) {
      throw new ApiError(404, "Drone not found");
    }
    return JSON.parse(JSON.stringify(drone));
  } catch (error) {
    throw new ApiError(error.code || error.statusCode || 500, error.message || error);
  }
};

const fetchAll = async (criteria = {}, options = {}) => {
  const { sort = { createdAt: -1 }, limit, page } = options;

  const _limit = parseInt(limit, 10);
  const _page = parseInt(page, 10);

  let drone = await Drone.find(criteria)
    .sort(sort)
    .limit(_limit)
    .skip(_limit * (_page - 1));

  return { drone, page: _page };
};

const count = async (criteria = {}) => {
  return await Drone.find(criteria).countDocuments();
};

const updateDrone = async (postId, data) => {
  let drone = await Drone.findById(postId);
  if (!drone || drone.isDeleted) {
    throw new ApiError(404, "Drone not found");
  }
  Object.assign(drone, data);
  await drone.save();
  return drone;
};

const deleteDrone = async (id) => {
  const drone = await Drone.findById(id);
  if (!drone) {
    throw new ApiError(404, "Drone not found");
  }

  Object.assign(drone, { isDeleted: true });
  await drone.save();
  return drone;
};

module.exports = {
  createDrone,
  findOne,
  fetchAll,
  count,
  updateDrone,
  deleteDrone,
};
