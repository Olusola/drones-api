const { Audit } = require("../models");


const fetchAll = async (criteria = {}, options = {}) => {
  const { sort = { createdAt: -1 }, limit, page } = options;

  const _limit = parseInt(limit, 10);
  const _page = parseInt(page, 10);

  let audit = await Audit.find(criteria)
    .sort(sort)
    .limit(_limit)
    .populate("medication")
    .populate("drone")
    .skip(_limit * (_page - 1));

  return { audit, page: _page };
};


module.exports = {
  fetchAll
};
