const catchAsync = require("../helpers/catchAsync");
const { medicationService } = require("../services");
const ApiError = require("../helpers/ApiError");
const pick = require("../helpers/pick");

const createMedication = catchAsync(async function (req, res) {
  const medication = await medicationService.createMedication(req);
  res.status(201).send({
    message: "Medication created successfully",
    data: {
      medication,
    },
  });
});

const edit = catchAsync(async function (req, res) {
  const medication = await medicationService.updateMedication(req.params._id, req);

  res.status(200).send({
    message: "Medication updated successfully",
    data: {
      medication,
    },
  });
});

const list = catchAsync(async function (req, res) {
  const filter = { isDeleted: false };
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const { medication, page } = await medicationService.fetchAll(filter, options);
  const count = await medicationService.count(filter);
  res.status(200).send({
    status: "success",
    message: "Medication Fetched successfully",
    data: {
      count,
      currentPage: page,
      medication,
    },
  });
});

const listOne = catchAsync(async function (req, res) {
  const medication = await medicationService.findOne({
    _id: req.params._id,
    isDeleted: false,
  });
  if (!medication) {
    throw new ApiError(404, "Medication not found");
  }
  res.status(200).send({
    status: "success",
    message: "Medication fetched Successfully",
    data: {
      medication,
    },
  });
});

const deleteMedication = catchAsync(async function (req, res) {
  const medication = await medicationService.deleteMedication(req.params._id);

  res.status(200).send({
    message: "Medication deleted successfully",
    data: {
      medication,
    },
  });
});

module.exports = {
  createMedication,
  edit,
  list,
  deleteMedication,
  listOne,
};
