const catchAsync = require("../helpers/catchAsync");
const { dispatchService, droneService, auditService } = require("../services");
const ApiError = require("../helpers/ApiError");
const pick = require("../helpers/pick");

const createDispatch = catchAsync(async function (req, res) {
  const dispatch = await dispatchService.createDispatch(req.body);
  res.status(201).send({
    message: "Dispatch created successfully",
    data: {
      dispatch,
    },
  });
});

const createDrone = catchAsync(async function (req, res) {
  const drone = await droneService.createDrone(req.body);
  res.status(201).send({
    message: "Drone created successfully",
    data: {
      drone,
    },
  });
});

const droneDetails = catchAsync(async function (req, res) {
  const drone = await droneService.findOne({
    _id: req.params._id,
    isDeleted: false,
  });;
  res.status(201).send({
    message: "Drone fetched successfully",
    data: {
      drone,
    },
  });
});

const availableDrones = catchAsync(async function (req, res) {
  const drone = await droneService.fetchAll({
    state: "IDLE",
    isDeleted: false,
  });;
  res.status(201).send({
    message: "Drones fetched successfully",
    data: {
      drone,
    },
  });
});

const auditTrail = catchAsync(async function (req, res) {
  const audit = await auditService.fetchAll();
  res.status(201).send({
    message: "Audit Trail fetched successfully",
    data: {
      audit,
    },
  });
});

const list = catchAsync(async function (req, res) {
  const filter = { isDeleted: false };
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const { dispatch, page } = await dispatchService.fetchAll(filter, options);
  const count = await dispatchService.count(filter);
  res.status(200).send({
    status: "success",
    message: "dispatch Fetched successfully",
    data: {
      count,
      currentPage: page,
      dispatch,
    },
  });
});

const listOne = catchAsync(async function (req, res) {
  const dispatch = await dispatchService.findOne({
    _id: req.params._id,
    isDeleted: false,
  });
  if (!dispatch) {
    throw new ApiError(404, "dispatch not found");
  }
  res.status(200).send({
    status: "success",
    message: "dispatch fetched Successfully",
    data: {
      dispatch,
    },
  });
});

const findDrone = catchAsync(async function (req, res) {
  const dispatch = await dispatchService.findOne({
    drone: req.params._id,
    isDeleted: false,
  });
  if (!dispatch) {
    throw new ApiError(404, "dispatch not found");
  }
  res.status(200).send({
    status: "success",
    message: "dispatch fetched Successfully",
    data: {
      dispatch,
    },
  });
});

const deleteDispatch = catchAsync(async function (req, res) {
  const dispatch = await dispatchService.deleteDispatch(req.params._id);

  res.status(200).send({
    message: "dispatch deleted successfully",
    data: {
      dispatch,
    },
  });
});

module.exports = {
  createDispatch,
  list,
  deleteDispatch,
  listOne,
  findDrone,
  createDrone,
  droneDetails,
  availableDrones,
  auditTrail
};
