const catchAsync = require("../helpers/catchAsync");
const { droneService } = require("../services");
const ApiError = require("../helpers/ApiError");
const pick = require("../helpers/pick");

const createDrone = catchAsync(async function (req, res) {
  const drone = await droneService.createDrone(req.body);
  res.status(201).send({
    message: "Drone created successfully",
    data: {
      drone,
    },
  });
});

const edit = catchAsync(async function (req, res) {
  const drone = await droneService.updateDrone(req.params._id, req.body);

  res.status(200).send({
    message: "Drone updated successfully",
    data: {
      drone,
    },
  });
});

const list = catchAsync(async function (req, res) {
  const filter = { isDeleted: false };
  const options = pick(req.query, ["sortBy", "limit", "page"]);
  const { drone, page } = await droneService.fetchAll(filter, options);
  const count = await droneService.count(filter);
  res.status(200).send({
    status: "success",
    message: "Drone Fetched successfully",
    data: {
      count,
      currentPage: page,
      drone,
    },
  });
});

const listOne = catchAsync(async function (req, res) {
  const drone = await droneService.findOne({
    _id: req.params._id,
    isDeleted: false,
  });
  if (!drone) {
    throw new ApiError(404, "Drone not found");
  }
  res.status(200).send({
    status: "success",
    message: "Drone fetched Successfully",
    data: {
      drone,
    },
  });
});

const deleteDrone = catchAsync(async function (req, res) {
  const drone = await droneService.deleteDrone(req.params._id);

  res.status(200).send({
    message: "Drone deleted successfully",
    data: {
      drone,
    },
  });
});

module.exports = {
  createDrone,
  edit,
  list,
  deleteDrone,
  listOne,
};
