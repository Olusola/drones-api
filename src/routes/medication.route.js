const express = require("express");
const validate = require("../helpers/validate");

const { medicationController } = require("../controllers");
const { medicationValidation } = require("../policies");

const router = express.Router();

router.post(
  "/create",
  [validate(medicationValidation.create)],
  medicationController.createMedication
);

router.put(
  "/update/:_id",
  [validate(medicationValidation.get)],
  medicationController.edit
);

router.delete(
  "/delete/:_id",
  medicationController.deleteMedication
);

router.get("/:_id", [validate(medicationValidation.get)], medicationController.listOne);

router.get("/", medicationController.list);

module.exports = router;
