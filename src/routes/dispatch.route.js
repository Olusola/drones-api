const express = require("express");
const validate = require("../helpers/validate");

const { dispatchController } = require("../controllers");
const { dispatchValidation, droneValidation } = require("../policies");

const router = express.Router();

router.post(
  "/create",
  [validate(dispatchValidation.create)],
  dispatchController.createDispatch
);

router.post(
  "/drone/create",
  [validate(droneValidation.create)],
  dispatchController.createDrone
);

router.get(
  "/drone/available",
  dispatchController.availableDrones
);

router.get(
  "/drone/:_id",
  [validate(dispatchValidation.get)],
  dispatchController.findDrone
);

router.get(
  "/drone/details/:_id",
  [validate(dispatchValidation.get)],
  dispatchController.droneDetails
);

router.delete(
  "/delete/:_id",
  dispatchController.deleteDispatch
);

router.get("/audit", dispatchController.auditTrail);

router.get("/:_id", [validate(dispatchValidation.get)], dispatchController.listOne);

router.get("/", dispatchController.list);

module.exports = router;
