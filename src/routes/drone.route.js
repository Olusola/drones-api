const express = require("express");
const validate = require("../helpers/validate");

const { droneController } = require("../controllers");
const { droneValidation } = require("../policies");

const router = express.Router();

router.post(
  "/create",
  [validate(droneValidation.create)],
  droneController.createDrone
);

router.put(
  "/update/:_id",
  [validate(droneValidation.get)],
  droneController.edit
);

router.delete(
  "/delete/:_id",
  droneController.deleteDrone
);

router.get("/:_id", [validate(droneValidation.get)], droneController.listOne);

router.get("/", droneController.list);

module.exports = router;
