const express = require("express");
const router = new express.Router();

const medicationRouter = require("./medication.route.js");
const droneRouter = require("./drone.route.js");
const dispatchRouter = require("./dispatch.route.js");

router.use("/medication", medicationRouter);
router.use("/drone", droneRouter);
router.use("/dispatch", dispatchRouter);

module.exports = router;
