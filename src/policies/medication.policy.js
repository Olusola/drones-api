const Joi = require("@hapi/joi");
const codePattern = /^[A-Z0-9_]*$/
const namePattern = /^[A-Za-z0-9_-]*$/
const mongoIdPattern = /^[0-9a-fA-F]{24}$/

const create = {
  body: Joi.object().keys({
    name: Joi.string().pattern(namePattern).required().messages({
      "string.empty": `Medication Name cannot be an empty field`,
      "any.required": `Medication Name is a required field`,
      "any.pattern": `Code can only contain alphabets, numbers, dashes and underscores`,
    }),
    weight: Joi.number().max(500).required().messages({
      "any.empty": `Weight cannot be an empty field`,
      "number.max": `Weight should have a maximum length of {#limit}`,
      "any.required": `Weight is a required field`,
    }),
    code: Joi.string().pattern(codePattern).required().messages({
      "any.empty": `Code cannot be an empty field`,
      "string.pattern": `Code can only contain capital, numbers and underscores`,
      "any.required": `Code is a required field`,
    }),
  }),
};

const get = {
  params: Joi.object().keys({
    _id: Joi.string().pattern(mongoIdPattern).required().messages({
      "string.empty": `Medication ID Must be passed cannot be an empty field`,
      "any.required": `Medication ID is a required field`,
      "string.pattern": `Medication ID must be a valid Mongo ID`,
    }),

  }),
};

module.exports = {
  create,
  get
};
