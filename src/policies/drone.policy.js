const Joi = require("@hapi/joi");
const codePattern = /^[A-Z0-9_]*$/
const mongoIdPattern = /^[0-9a-fA-F]{24}$/
const { DRONE_MODELS, DRONE_STATES } = "../helpers/constants"

const create = {
  body: Joi.object().keys({
    serialNumber: Joi.string().max(500).required().messages({
      "string.empty": `Drone Serial Number cannot be an empty field`,
      "any.required": `Drone Serial Number is a required field`,
      "string.max": `Drone Serial Number should have a maximum length of {#limit}`,
    }),
    model: Joi.string().valid("LIGHTWEIGHT", "MxIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT").required().messages({
      "any.empty": `Model cannot be an empty field`,
      "string.valid": `Invalid model passed`,
      "any.required": `Model is a required field`,
    }),
    state: Joi.string().valid("IDLE", "LOADING", "LOADED", "DELIVERING", "DELIVERED", "RETURNING").required().messages({
      "any.empty": `State cannot be an empty field`,
      "string.valid": `Invalid state passed`,
      "any.required": `State is a required field`,
    }),
    maxWeight: Joi.number().required().messages({
      "number.empty": `Max Weight cannot be an empty field`,
      "any.required": `Max Weight is a required field`,
    }),
  }).unknown(),
};

const get = {
  params: Joi.object().keys({
    _id: Joi.string().pattern(mongoIdPattern).required().messages({
      "string.empty": `Drone ID Must be passed cannot be an empty field`,
      "any.required": `Drone ID is a required field`,
      "string.pattern": `Drone ID must be a valid Mongo ID`,
    }),

  }),
};

module.exports = {
  create,
  get
};
