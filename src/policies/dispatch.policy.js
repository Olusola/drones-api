const Joi = require("@hapi/joi");
const codePattern = /^[A-Z0-9_]*$/
const mongoIdPattern = /^[0-9a-fA-F]{24}$/

const create = {
  body: Joi.object().keys({
    drone: Joi.string().pattern(mongoIdPattern).required().messages({
      "string.empty": `Drone ID Must be passed cannot be an empty field`,
      "any.required": `Drone ID is a required field`,
      "string.pattern": `Drone ID must be a valid Mongo ID`,
    }),
    // medication: Joi.array().min(1).required().messages({
    //   "string.empty": `You need to attach at least one medication`,
    //   "any.required": `You need to attach at least one medication`,
    // }),
  }).unknown(),
};

const get = {
  params: Joi.object().keys({
    _id: Joi.string().pattern(mongoIdPattern).required().messages({
      "string.empty": `Drone ID Must be passed cannot be an empty field`,
      "any.required": `Drone ID is a required field`,
      "string.pattern": `Drone ID must be a valid Mongo ID`,
    }),

  }),
};

module.exports = {
  create,
  get
};
