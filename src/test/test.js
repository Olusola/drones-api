require("dotenv").config();
process.env.PORT = 7777;
process.env.DB_CONN = `${process.env.DB_CONN}_test`;
const logger = require("../helpers/logger");
const { faker } = require('@faker-js/faker');

const mongoose = require("mongoose");
let app = require("../index");
let chai = require("chai");
let chaiHttp = require("chai-http");
let should = chai.should();
let expect = chai.expect;
let assert = chai.assert;
const { Drone, Medication } = require("../models");

chai.use(chaiHttp);

const connectDB = async () => {
  logger.info("connecting to db");

  await mongoose.connect(process.env.DB_CONN, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
};

const clearDB = async () => {
  logger.warn("clearing db");
  await Promise.all(
    Object.values(mongoose.connection.collections).map(async (collection) =>
      collection.deleteMany()
    )
  );
};

//Our parent block
describe("Authentication", async () => {
  beforeEach((done) => {
    //Before each test we empty the database
    // connectDB();
    // clearDB();
    done();
  });


  describe("/POST Create Drone", () => {
    it("Successfully create a drone", async (done) => {
      const drone = {
        serialNumber: faker.random.numeric(8),
        model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
        maxWeight: faker.random.numeric(3),
        batteryLevel: 100,
        state: "LOADING"
      };
      chai
        .request(app)
        .post("/api/v1/dispatch/drone/create")
        .send(drone)
        .end((err, res) => {
          res.should.have.status(201);
          assert.isDefined(res.body.data);
          assert.isDefined(res.body.message);
          done();
        });
    });
  });

  describe("/POST Create Dispatch", () => {
    it("Successfully load a drone with medication items", async (done) => {
      const medicationData = [];
      // eslint-disable-next-line no-plusplus
      for (let i = 1; i <= 5; i++) {
        const medication = {
          name: `${faker.lorem.word()}-${faker.random.numeric(5)}`,
          code: `${faker.lorem.word().toUpperCase()}-${faker.random.numeric(5)}`,
          images: [faker.image.imageUrl()],
          weight: faker.random.numeric(2),
        };

        medicationData.push(medication);
      }
      const medication = await Medication.create(medicationData);

      const drone = {
        serialNumber: faker.random.numeric(8),
        model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
        maxWeight: faker.random.numeric(3),
        batteryLevel: 100,
        state: "IDLE"
      };
      const createdDrone = await Drone.create(drone);
      const data = {
        drone: createdDrone._id, medication: medication.map(med => {
          return med._id
        })
      }
      chai
        .request(app)
        .post("/api/v1/dispatch/create")
        .send(data)
        .end((err, res) => {
          res.should.have.status(201);
          assert.isDefined(res.body.data);
          assert.isDefined(res.body.message);
          done();
        });
    });
  });

  describe("/POST Create Dispatch", () => {
    it("Prevent a drone in use from loading", async (done) => {
      const medicationData = [];
      // eslint-disable-next-line no-plusplus
      for (let i = 1; i <= 2; i++) {
        const medication = {
          name: `${faker.lorem.word()}-${faker.random.numeric(5)}`,
          code: `${faker.lorem.word().toUpperCase()}-${faker.random.numeric(5)}`,
          images: [faker.image.imageUrl()],
          weight: faker.random.numeric(2),
        };

        medicationData.push(medication);
      }
      const medication = await Medication.create(medicationData);

      const drone = {
        serialNumber: faker.random.numeric(8),
        model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
        maxWeight: faker.random.numeric(10),
        batteryLevel: 100,
        state: "LOADING"
      };
      const createdDrone = await Drone.create(drone);
      const data = {
        drone: createdDrone._id, medication: medication.map(med => {
          return med._id
        })
      }
      chai
        .request(app)
        .post("/api/v1/dispatch/create")
        .send(data)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST Create Dispatch", () => {
    it("Prevent a drone with a battery level less than 25% from loading", async (done) => {
      const medicationData = [];
      // eslint-disable-next-line no-plusplus
      for (let i = 1; i <= 2; i++) {
        const medication = {
          name: `${faker.lorem.word()}-${faker.random.numeric(5)}`,
          code: `${faker.lorem.word().toUpperCase()}-${faker.random.numeric(5)}`,
          images: [faker.image.imageUrl()],
          weight: faker.random.numeric(2),
        };

        medicationData.push(medication);
      }
      const medication = await Medication.create(medicationData);

      const drone = {
        serialNumber: faker.random.numeric(8),
        model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
        maxWeight: faker.random.numeric(10),
        batteryLevel: 10,
        state: "IDLE"
      };
      const createdDrone = await Drone.create(drone);
      const data = {
        drone: createdDrone._id, medication: medication.map(med => {
          return med._id
        })
      }
      chai
        .request(app)
        .post("/api/v1/dispatch/create")
        .send(data)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST Create Dispatch", () => {
    it("Prevent a drone from loading with more than its max weight", async (done) => {
      const medicationData = [];
      // eslint-disable-next-line no-plusplus
      for (let i = 1; i <= 5; i++) {
        const medication = {
          name: `${faker.lorem.word()}-${faker.random.numeric(5)}`,
          code: `${faker.lorem.word().toUpperCase()}-${faker.random.numeric(5)}`,
          images: [faker.image.imageUrl()],
          weight: faker.random.numeric(5),
        };

        medicationData.push(medication);
      }
      const medication = await Medication.create(medicationData);

      const drone = {
        serialNumber: faker.random.numeric(8),
        model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
        maxWeight: faker.random.numeric(1),
        batteryLevel: 10,
        state: "IDLE"
      };
      const createdDrone = await Drone.create(drone);
      const data = {
        drone: createdDrone._id, medication: medication.map(med => {
          return med._id
        })
      }
      chai
        .request(app)
        .post("/api/v1/dispatch/create")
        .send(data)
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});
