const mongoose = require("mongoose");
const logger = require("../helpers/logger");
require("dotenv").config();
const { Drone, Dispatch, Medication } = require("../models");
const { getDroneData } = require("./mock/drone.mock");
const { getDispatchData } = require("./mock/dispatch.mock");
const { getMedicationData } = require("./mock/medication.mock");

const connectDB = async () => {
  logger.info("connecting to db");

  await mongoose.connect(process.env.DB_CONN, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
};

const clearDB = async () => {
  logger.warn("clearing db");
  await Promise.all(
    Object.values(mongoose.connection.collections).map(async (collection) =>
      collection.deleteMany()
    )
  );
};

const disconnectDB = async () => {
  logger.warn("disconnecting from db");
  await mongoose.disconnect();
};

const seedDB = async () => {
  try {
    logger.info("Seeding Database");
    await connectDB();
    await clearDB();

    logger.info("Creating Random Drone");
    const fakeDrones = await Drone.create(
      await getDroneData()
    );

    logger.info("Creating Random Medication");
    const fakeMedication = await Medication.create(await getMedicationData());

    logger.info("Creating Random Dispatch");
    await Dispatch.create(await getDispatchData(fakeDrones, fakeMedication));

    logger.info("seeder completed");
  } catch (e) {
    logger.error(e);
  }

  await disconnectDB();
};

seedDB();
