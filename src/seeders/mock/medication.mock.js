// eslint-disable-next-line import/no-extraneous-dependencies
const { faker } = require('@faker-js/faker');

const getMedicationData = async () => {
  const medicationData = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= 200; i++) {
    const medication = {
      name: `${faker.lorem.word()}-${faker.random.numeric(5)}`,
      code: `${faker.lorem.word().toUpperCase()}-${faker.random.numeric(5)}`,
      images: [faker.image.imageUrl()],
      weight: faker.random.numeric(2),
    };

    medicationData.push(medication);
  }

  return medicationData;
};

module.exports = {
  getMedicationData,
};
