// eslint-disable-next-line import/no-extraneous-dependencies
const { faker } = require('@faker-js/faker');
const { DRONE_MODELS } = "../helpers/constants"

const getDroneData = async () => {
  const droneData = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= 200; i++) {
    const drone = {
      serialNumber: faker.random.numeric(8),
      model: faker.helpers.arrayElement(["LIGHTWEIGHT", "MIDDLEWEIGHT", "CRUISERWEIGHT", "HEAVYWEIGHT"]),
      maxWeight: faker.random.numeric(3),
      batteryLevel: 100,
      state: "LOADING"
    };

    droneData.push(drone);
  }

  return droneData;
};

module.exports = {
  getDroneData,
};
