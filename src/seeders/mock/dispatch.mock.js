// eslint-disable-next-line import/no-extraneous-dependencies
const { faker } = require('@faker-js/faker');

const getDispatchData = async (drones, medications) => {
  const dispatchdata = [];

  // eslint-disable-next-line no-plusplus
  for (let i = 1; i <= 200; i++) {
    const dispatch = {
      drone: drones[i - 1]._id,
      medication: [faker.helpers.arrayElement(medications),
      faker.helpers.arrayElement(medications),
      faker.helpers.arrayElement(medications),
      faker.helpers.arrayElement(medications), faker.helpers.arrayElement(medications)],
    };

    dispatchdata.push(dispatch);
  }

  return dispatchdata;
};

module.exports = {
  getDispatchData,
};
