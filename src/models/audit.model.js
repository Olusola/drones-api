const mongoose = require("mongoose");
let Schema = mongoose.Schema;

var auditSchema = new mongoose.Schema(
  {
    drone: {
      type: Schema.Types.ObjectId,
      ref: "Drone",
      required: true,
    },
    batteryLevel: {
      type: Number,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true, versionKey: false }
);

const Audit = mongoose.model("Audit", auditSchema);

module.exports = Audit;
