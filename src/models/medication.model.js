const mongoose = require("mongoose");

var medicationSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    weight: {
      type: Number,
      required: true,
    },
    code: {
      type: String,
      required: true,
    },
    images: [{ type: String }],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true, versionKey: false }
);

const Medication = mongoose.model("Medication", medicationSchema);

module.exports = Medication;
