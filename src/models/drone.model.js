const { DRONE_MODELS, DRONE_STATES } = "../helpers/constants"
const mongoose = require("mongoose");

var droneSchema = new mongoose.Schema(
  {
    serialNumber: {
      type: String,
      required: true,
    },
    model: {
      type: String,
      enum: DRONE_MODELS,
      required: true,
    },
    maxWeight: {
      type: Number,
      required: true,
    },
    batteryLevel: {
      type: Number,
      required: true,
    },
    state: {
      type: String,
      enum: DRONE_STATES,
      default: "IDLE",
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true, versionKey: false }
);

const Drone = mongoose.model("Drone", droneSchema);

module.exports = Drone;
