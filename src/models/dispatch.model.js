const mongoose = require("mongoose");
let Schema = mongoose.Schema;

var dispatchSchema = new mongoose.Schema(
  {
    drone: {
      type: Schema.Types.ObjectId,
      ref: "Drone",
      required: true,
    },
    medication: [{
      type: Schema.Types.ObjectId,
      ref: "Medication",
    }],
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true, versionKey: false }
);

const Dispatch = mongoose.model("Dispatch", dispatchSchema);

module.exports = Dispatch;
